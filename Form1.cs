﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace game
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int[,] a = new int[4, 4];
        int i,j;
        int var;
        int x=0;
        int y=0;

        private void Form1_Load(object sender, EventArgs e)
        {
            panel1.Visible = false;
            label4.Visible = false;
            label5.Visible = false;
            label5.Text = "0";
            label4.Text = "0";
        }

        private void newgamebutton_Click(object sender, EventArgs e)
        {
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = true;
            button4.Enabled = true;
            button5.Enabled = true;
            button6.Enabled = true;
            button7.Enabled = true;
            button8.Enabled = true;
            button9.Enabled = true;

            button1.Text = "";
            button2.Text = "";
            button3.Text = "";
            button4.Text = "";
            button5.Text = "";
            button6.Text = "";
            button7.Text = "";
            button8.Text = "";
            button9.Text = "";

            for (i = 1; i <= 3; i++)
            {
                for (j = 1; j <= 3; j++)
                {
                    a[i, j] = 0;
                }
            }

            var = 0;

            panel1.Visible = true;
            panel1.Enabled = true;
          
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (var % 2 == 0)
            {
                button1.Text = "x";
                a[1, 1] = 1;
            }
            else
            {
                button1.Text = "0";
                a[1 , 1] = 2;
            }
            verificare();
            button1.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (var % 2 == 0)
            {
                button2.Text = "x";
                a[1, 2] = 1;
            }
            else
            {
                button2.Text = "0";
                a[1, 2] = 2;
            }
            verificare();
            button2.Enabled = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (var % 2 == 0)
            {
                button3.Text = "x";
                a[1, 3] = 1;
            }
            else
            {
                button3.Text = "0";
                a[1, 3] = 2;
            }
            verificare();
            button3.Enabled = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (var % 2 == 0)
            {
                button4.Text = "x";
                a[2, 1] = 1;
            }
            else
            {
                button4.Text = "0";
                a[2, 1] = 2;
            }
            verificare();
            button4.Enabled = false;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (var % 2 == 0)
            {
                button5.Text = "x";
                a[2, 2] = 1;
            }
            else
            {
                button5.Text = "0";
                a[2, 2] = 2;
            }
            verificare();
            button5.Enabled = false;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (var % 2 == 0)
            {
                button6.Text = "x";
                a[2, 3] = 1;
            }
            else
            {
                button6.Text = "0";
                a[2, 3] = 2;
            }
            verificare();
            button6.Enabled = false;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (var % 2 == 0)
            {
                button7.Text = "x";
                a[3, 1] = 1;
            }
            else
            {
                button7.Text = "0";
                a[3, 1] = 2;
            }
            verificare();
            button7.Enabled = false;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (var % 2 == 0)
            {
                button8.Text = "x";
                a[3, 2] = 1;
            }
            else
            {
                button8.Text = "0";
                a[3, 2] = 2;
            }
            verificare();
            button8.Enabled = false;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (var % 2 == 0)
            {
                button9.Text = "x";
                a[3, 3] = 1;
            }
            else
            {
                button9.Text = "0";
                a[3, 3] = 2;
            }
            verificare();
            button9.Enabled = false;
        }
        public void verificare()
        {
            if((a[1,1]==1 && a[1,2]==1 && a[1,3]==1) || 
               (a[2,1]==1 && a[2,2]==1 && a[2,3]==1) || 
               (a[3,1]==1 && a[3,2]==1 && a[3,3]==1) ||
               (a[1,1]==1 && a[2,1]==1 && a[3,1]==1) ||
               (a[1,2]==1 && a[2,2]==1 && a[3,2]==1) ||
               (a[1,3]==1 && a[2,3]==1 && a[3,3]==1) ||
               (a[1,1]==1 && a[2,2]==1 && a[3,3]==1) ||
               (a[1,3]==1 && a[2,2]==1 && a[3,1]==1))
            {
                MessageBox.Show("X wins.");
                panel1.Enabled = false;
                label4.Visible = true;
                label5.Visible = true;
                label4.Text = Convert.ToString(x+1);
                x++;
            }
            else if ((a[1, 1]== 2 && a[1, 2] == 2 && a[1, 3] == 2) ||
                    (a[2, 1] == 2 && a[2, 2] == 2 && a[2, 3] == 2) ||
                    (a[3, 1] == 2 && a[3, 2] == 2 && a[3, 3] == 2) ||
                    (a[1, 1] == 2 && a[2, 1] == 2 && a[3, 1] == 2) ||
                    (a[1, 2] == 2 && a[2, 2] == 2 && a[3, 2] == 1) ||
                    (a[1, 3] == 2 && a[2, 3] == 2 && a[3, 3] == 2) ||
                    (a[1, 1] == 2 && a[2, 2] == 2 && a[3, 3] == 2) ||
                    (a[1, 3] == 2 && a[2, 2] == 2 && a[3, 1] == 2))
            {
                MessageBox.Show("0 Wins.");
                panel1.Enabled = false;
                label4.Visible = true;
                label5.Visible = true;
                label5.Text = Convert.ToString(y + 1);
                y++;
                
            }
            else
            {
                var++;
                if (var == 9)
                {
                    MessageBox.Show("Draw");
                    panel1.Enabled = false;
                }
                
            }
       
        }

    }
}
